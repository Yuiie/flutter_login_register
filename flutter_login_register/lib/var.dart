import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class localVar{

  Future<File> writeContent(String token) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    final File file = File('${directory.path}/my_file.txt');
    await file.writeAsString(token);
  }

  Future<File> deleteFile()async {
    final Directory directory = await getApplicationDocumentsDirectory();
    directory.deleteSync(recursive: true);
  }

  Future<String> readContent() async {

    String text;
    try {
      final Directory directory = await getApplicationDocumentsDirectory();
      final File file = File('${directory.path}/my_file.txt');
      text = await file.readAsString();
    } catch (e) {
      print("Couldn't read file");
    }
    return text;
  }
}
