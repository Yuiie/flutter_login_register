class RegisterJson {
  final String error;
  final String code;

  RegisterJson._({this.error, this.code});

  factory RegisterJson.fromJson(Map<String, dynamic> json) {
    return new RegisterJson._(
      error: json['error'],
      code: json['code'],
    );
  }
}

