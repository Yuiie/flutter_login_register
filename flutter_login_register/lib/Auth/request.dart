import 'package:flutterloginregister/Auth/login_request.dart';
import 'package:flutterloginregister/Auth/register_request.dart';
import 'package:flutterloginregister/global.dart' as global;
import 'package:flutterloginregister/var.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Request{
  static login(String user, String password) async {
    List<LoginJson> list = List();
    String url = "http://api.airi.ovh/user/create/token/"+user+"&="+password;
    print("my login "+url);
    final response =
    await http.get(url);
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new LoginJson.fromJson(data))
          .toList();
      global.error = list[0].error;
      localVar().writeContent(list[0].token);
    } else {
      throw Exception('Failed to access url');
    }
  }

  static silentLogin() async {
    List<LoginJson> list = List();

    global.token = await localVar().readContent();
    String url = "http://api.airi.ovh/user/login/"+global.token;
    print("my silent login "+url);
    final response =
    await http.get(url);
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new LoginJson.fromJson(data))
          .toList();
      global.error = list[0].error;
      global.code = list[0].code;
      global.token = list[0].token;
    } else {
      throw Exception('Failed to Silent login');
    }
  }

  static register(String user, String email, String password) async {
    List<RegisterJson> list = List();

    String url = "http://api.airi.ovh/user/create/"+user+"&="+email+"&="+password;
    final response =
    await http.get(url);
    if (response.statusCode == 200) {
      list = (json.decode(response.body) as List)
          .map((data) => new RegisterJson.fromJson(data))
          .toList();
      global.error = list[0].error;
    } else {
      throw Exception('Failed to access url');
    }
  }
}