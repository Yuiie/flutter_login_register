import 'package:flutter/material.dart';
import 'package:flutterloginregister/Auth/request.dart';
import 'package:flutterloginregister/global.dart' as global;
import 'package:flutterloginregister/var.dart';

class LoginPage extends StatefulWidget {
  static final String path = "lib/src/pages/login/auth3.dart";

  @override
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {

  void imauth() async {
    await localVar().readContent();
    await Request.silentLogin();
    if (global.error == "success") {
      print("connecté avec silent login");
    }
  }

  @override
  void initState() {
    super.initState();
    imauth();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar:  new AppBar(),
        body: Container(
          child: Column(
            children: <Widget>[
              new Expanded(
                child: SignupForm(),
              ),
              const SizedBox(height: 10.0),
              new Expanded(
                child: LoginForm(),
              )
            ],
          ),
        ));
  }
}

class LoginForm extends StatelessWidget {
  const LoginForm({
    Key key,
  }) : super(key: key);


  checkLogin(context, userController, passwordController) async {
    if (userController.text.isNotEmpty && passwordController.text.isNotEmpty) {
      await Request.login(userController.text, passwordController.text);
    }
    else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Remplir tous les champs"),
        duration: Duration(seconds: 3),
      ));
    }
    if (global.error == "success") {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Connecté"),
        duration: Duration(seconds: 3),
      ));
    }
    else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Mauvais identifiants"),
        duration: Duration(seconds: 3),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final userController = TextEditingController();
    final passwordController = TextEditingController();
    return Container(
      margin: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: ListView(
        children: <Widget>[
          TextField(
            controller: userController,
            decoration: InputDecoration(
              hintText: "Enter username",
              border: OutlineInputBorder(),
            ),
          ),
          TextField(
            obscureText: true,
            controller: passwordController,
            decoration: InputDecoration(
              hintText: "Enter password",
              border: OutlineInputBorder(),
            ),
          ),
          RaisedButton(
            child: Text("Login"),
            onPressed: () {
              checkLogin(context, userController, passwordController);
            },
          ),
        ],
      ),
    );
  }
}

class SignupForm extends StatelessWidget {
  const SignupForm({
    Key key,
  }) : super(key: key);

  checkRegister(context, userController, emailController, passwordController) async {
    if (userController.text.isNotEmpty && emailController.text.isNotEmpty && passwordController.text.isNotEmpty) {
      await Request.register(userController.text, emailController.text, passwordController.text);
    }
    else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Remplir tous les champs"),
        duration: Duration(seconds: 3),
      ));
    }
    if (global.error == "success") {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Inscription reussie"),
        duration: Duration(seconds: 3),
      ));
    }
    else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(global.error),
        duration: Duration(seconds: 3),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final userController = TextEditingController();
    final emailController = TextEditingController();
    final passwordController = TextEditingController();
    final passwordController1 = TextEditingController();
    return Container(
      margin: const EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(16.0),
        children: <Widget>[
          TextField(
            controller: userController,
            decoration: InputDecoration(
              hintText: "Enter username",
              border: OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 10.0),
          TextField(
            controller: emailController,
            decoration: InputDecoration(
              hintText: "Enter email",
              border: OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 10.0),
          TextField(
            obscureText: true,
            controller: passwordController,
            decoration: InputDecoration(
              hintText: "Enter password",
              border: OutlineInputBorder(),
            ),
          ),
          const SizedBox(height: 10.0),
          RaisedButton(
            color: Colors.red,
            textColor: Colors.white,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Text("Signup"),
            onPressed: () {
              checkRegister(context, userController, emailController, passwordController);
            },
          ),
        ],
      ),
    );
  }
}