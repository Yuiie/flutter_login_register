  class LoginJson {
  final String error;
  final String code;
  final String token;

  LoginJson._({this.error, this.code,this.token});

  factory LoginJson.fromJson(Map<String, dynamic> json) {
    return new LoginJson._(
        error: json['error'],
        code: json['code'],
        token: json['token']
    );
  }
}
